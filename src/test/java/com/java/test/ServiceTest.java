package com.java.test;
import static org.junit.Assert.assertEquals;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.junit.Before;
import org.junit.Test;

import com.java.partition.IService;
import com.java.partition.Service;

public class ServiceTest {
	
	private IService<Integer> serviceInteger;
	private IService<String> serviceString;
	private List<Integer> listInteger;
	private List<String> listString;
	@Before
	public void setData() {
		serviceInteger=new Service<Integer>();
		serviceString=new Service<String>();
		listInteger=new ArrayList<Integer>(Arrays.asList(1,5,6,9));
		listString=new ArrayList<String>(Arrays.asList("a","b","c","d","k"));
	}
	@Test
	public void testPartition() {
		
		List<List<String>> listExpectedString=new ArrayList<List<String>>();
		listExpectedString.add(new ArrayList<String>(Arrays.asList("a","b","c")));
		listExpectedString.add(new ArrayList<String>(Arrays.asList("d","k")));
		assertEquals(listExpectedString, serviceString.partition(listString, 3));
		
		List<List<Integer>> listExpectedInteger=new ArrayList<List<Integer>>();
		listExpectedInteger.add(new ArrayList<Integer>(Arrays.asList(1,5,6)));
		listExpectedInteger.add(new ArrayList<Integer>(Arrays.asList(9)));
		assertEquals(listExpectedInteger, serviceInteger.partition(listInteger, 3));
		
		assertEquals(0, serviceInteger.partition(new ArrayList<Integer>(), 3).size());
		
	}
}
