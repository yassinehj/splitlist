package com.java.partition;
import java.util.List;

public interface IService<T> {
	
	/**
	 * This function will split a list to subLists of same sizes 
	 * 
	 * @param liste : The list that will splitted 
	 * @param taille : The max length of the subList
	 * @return List of subList 
	 */
	public List<List<T>> partition(List<T> liste,int taille);
}
