package com.java.partition;
import java.util.ArrayList;
import java.util.List;

/**
 * @author yhajji
 *
 * @param <T> The generic type of list's elements
 */
public class Service<T> implements IService<T>{
	
	
	public  List<List<T>> partition(List<T> liste, int taille) {
		if(liste.size()==0 || liste ==null)
			return new ArrayList<List<T>>();
		List<List<T>> resultat = new ArrayList<List<T>>();
	    final int N = liste.size();
	    for (int i = 0; i < N; i += taille) {
	    	resultat.add(new ArrayList<T>(
	            liste.subList(i, Math.min(N, i + taille)))
	        );
	    }
	    return resultat;
	}

}
